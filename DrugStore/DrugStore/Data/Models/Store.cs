﻿using System;

namespace DrugStore.Data.Models
{
    public class Store
    {
        public int Id { get; set; }

        public int DrugId { get; set; }

        public Drug Drug { get; set; }

        public decimal Amount { get; set; }

        public DateTime TimeOfAdding { get; set; }
    }
}
