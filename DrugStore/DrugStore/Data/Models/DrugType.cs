﻿namespace DrugStore.Data.Models
{
    public class DrugType
    {
        public int Id { get; set; }

        // Антибиотик, противоаллерген 
        public string Description { get; set; }
    }
}
