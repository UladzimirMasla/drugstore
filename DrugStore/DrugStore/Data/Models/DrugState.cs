﻿namespace DrugStore.Data.Models
{
    public class DrugState
    {
        public int Id { get; set; }

        // Капли, таблетки, порошок 
        public string Description { get; set; }
    }
}
