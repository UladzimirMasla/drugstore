﻿
namespace DrugStore.Data.Models
{
    public class Drug
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public decimal Cost { get; set; }

        public int ShelfLifeInDays { get; set; }

        public int DrugStateId { get; set; }

        public DrugState DrugState { get; set; }

        public int DrugTypeId { get; set; }

        public DrugType DrugType { get; set; }
    }
}
