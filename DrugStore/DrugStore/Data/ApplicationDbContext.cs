﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

using DrugStore.Data.Models;

namespace DrugStore.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {

        }


        public DbSet<Drug> Drugs { get; set; }

        public DbSet<DrugType> DrugTypes { get; set; }

        public DbSet<DrugState> DrugStates { get; set; }

        public DbSet<Store> Stores { get; set; }

        public DbSet<Provider> Providers { get; set; }
    }
}
